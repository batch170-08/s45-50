/*
code to create react app (command in gitbash/terminal)
  npx create-react-app <appName>
*/
/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/
// set up/import dependencies
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import App from './App.js'
/*import { BrowserRouter as Router, Route, Routes } from 'react-router-dom' // Setting up specific route*/

/*
  install bootstrap and react bootstrap first: npm install bootstrap@4.6.0 react-bootstrap@1.5.2
*/
/*// bootstrap dependencies
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';*/

// App Components
/*
  every component has to be imported before it can be rendered inside the index.js

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login  from './pages/Login.js';

*/


/*
ReactDOM.render()
  responsible for injecting/inserting the whole React.js Project inside the webpage
*/
/*
  depricated version of the render() in react
*/
// React.createElement('h1', null, 'Hello World');

/*
  JSX - JavaScript XML - is an extention of JS that let's us create objects which will then be compiled and added as HTML elements

  With JSX..
    -we are able to create HTML elements using JS
    -we are also able to create JS objects that will then be compiled and added as HTML elements

*/

/*
  Fragment - used to render different components inside the index.js. without it, the webpage will return errors since, it is the of JS to display two or more components in the frontend
    <>
    </>
      -also accepted in place of the Fragment but not all browsers are able to read this. also this does not support keys or attributes
*/
/*
  npm start - to start/launch the react app
*/
ReactDOM.render(
  <App />,
  document.getElementById('root')
);
