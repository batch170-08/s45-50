// dependencies
import  React, { Fragment } from 'react';


// App Components
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home() {

  const data = {
      title: "Zuitt Coding Bootcamp",
      content: "Opportunities for everyone, everywhere",
      destination: "/courses",
      label: "Enroll now!"
  }

  return (
    <Fragment>
      <Banner data={data}/>
      <Highlights />
    </Fragment>
  )
}
