/*
	import:
		react, useState, useEffect from react

		Form, Container, Button from react-bootstrap
*/
// Base Imports
import	React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';

// App Imports
import UserContext from '../userContext'

// Bootstrap
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2 to install sweetalert2

export default function Register(){

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobile, setMobile ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ passwordConfirm, setPasswordConfirm ] = useState('');
	const [ isDisabled, setIsDisabled ] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isMobileNotEmpty = mobile !== '';
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if ( isFirstNameNotEmpty && isLastNameNotEmpty && isMobileNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ firstName, lastName, mobile, email, password, passwordConfirm ]);

/*
	TWO_WAY BINDING

		To be able to capture/save the input value from the input elements, we can bind the value of the element with the states. We, as devs, cannot type into the the inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that is bound to the input

		Two-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we don't have to save it before submitting.

		"e.target.value"
		e - the event to which the element will listen
		target - the element where the event will happen
		value - the value that the user has entered in that element

*/



	function register(e){
		e.preventDefault();

		fetch('http://localhost:4000/api/users/checkEmail', {
			method:"POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
            	email: email
            })
		})
		.then(response => response.json())
		.then(data => {
			if(data === false) {
				fetch('http://localhost:4000/api/users/register', {
						method:"POST",
						headers: {"Content-Type": "application/json"},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobile: mobile,
							email: email,
							password: password,
							passwordConfirm: passwordConfirm
					})
				})
				.then(response => response.json())
				.then(data => {
					if (data === true) {
						setUser({access: data.access})
						Swal.fire("Registration successful")
						/*setFirstName('')
						setLastName('')
						setMobile('')
						setEmail('')
						setPassword('')
						setPasswordConfirm('')*/
						navigate("/login");
					} else {
						alert(data.error)
					}
				})
			} else {
				Swal.fire("Duplicate email found")
			}
		})
	}

//ACTIVITY
/*
	if (user.access !== null) {
		return <Navigate replace to="/courses" />
	}
*/

	return(
			<Container>
				<Form onSubmit={register}>
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="tel" placeholder="Enter your mobile number" value={mobile} pattern="[0-9]{11}" onChange={(e) => setMobile(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm password</Form.Label>
						<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
					</Form.Group>
					<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
		)
}

